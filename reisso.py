################################################################################
# reisso

# example problem: Bolognese, serves 2, ingredients: 
# 200g lean steak mince
# 1 onion (170g)
# 1 red pepper (160g)
# 150g mushrooms (chestnut or ordinary)
# 2 tablespoons tomato puree (30g)
# chicken stock cube (10g)
# 400g tin chopped tomatoes
# ½ teaspoon Worcestershire sauce (2.5g)
# 100g wholewheat spaghetti
################################################################################

import numpy as np
from pulp import *

# user entered parameters
userCals = 800             # desired calories of meal
macrosPercent = np.array([0.4, 0.3, 0.3]) # macros percentage (carbs, fat, protein)

# Reicipe Ingredient parameters
W = np.array([200, 170, 160, 150, 30, 10, 400, 2.5, 100])   # original weight of ingredients
A = np.array([191, 40, 31, 8, 9, 310, 23, 100, 350])
B = np.array([  [1,12,21],[9,0,1],[6,0,1],[4,0,2],[1,0,0],
                [38,16,3],[4,0,1],[23,0,1],[66,2,13]])

# rewrite as dictionaries for pulp
#Ingredients = [ "Steak", "Onion", "Pepper", "Mushrooms", "Puree", "Stock", "Tomatoe", 
#                "Worcestershire", "Spaghetti"]
#
#origWeight = {"Steak":200,"Onion":170, "Pepper":160, "Mushrooms":150, "Puree":30, "Stock":10, "Tomatoe":400, 
#                "Worcestershire":2.5, "Spaghetti":100}
#
#ingrCals = {"Steak":191,"Onion":40, "Pepper":31, "Mushrooms":8, "Puree":9, "Stock":310, "Tomatoe":23, 
#                "Worcestershire":100, "Spaghetti":350}
#
#ingrCarbs = {"Steak":1,"Onion":9, "Pepper":6, "Mushrooms":4, "Puree":1, "Stock":38, "Tomatoe":4, 
#                "Worcestershire":23, "Spaghetti":66}
#
#ingrFat = {"Steak":12,"Onion":0, "Pepper":0, "Mushrooms":0, "Puree":0, "Stock":16, "Tomatoe":0, 
#                "Worcestershire":0, "Spaghetti":2}
#
#ingrProtein = {"Steak":21,"Onion":1, "Pepper":1, "Mushrooms":2, "Puree":0, "Stock":3, "Tomatoe":1, 
#                "Worcestershire":1, "Spaghetti":13}

Ingredients = ["Protein Powder", "Yoghurt", "banana"]
origWeight = {"Protein Powder": 45, "Yoghurt": 150, "banana": 120}
ingrCals = {"Protein Powder": 370, "Yoghurt": 122, "banana": 89}
ingrCarbs = {"Protein Powder": 3.6, "Yoghurt": 3.2, "banana": 23}
ingrFat = {"Protein Powder": 3.7, "Yoghurt": 10, "banana": 0.3}
ingrProtein = {"Protein Powder": 80, "Yoghurt": 4.6, "banana": 1.1}


totalCals = sum(ingrCals[j]*origWeight[j] for j in ingrCals)
print("Total Calories:", totalCals/100)

origPercent = {Ingredients[i]: origWeight[Ingredients[i]]*ingrCals[Ingredients[i]] / totalCals for i in range(len(Ingredients))} 
ingrCarbsP = {Ingredients[i]: ingrCarbs[Ingredients[i]]*4*(1/ingrCals[Ingredients[i]]) for i in range(len(Ingredients))}
ingrFatP = {Ingredients[i]: ingrFat[Ingredients[i]]*9*(1/ingrCals[Ingredients[i]]) for i in range(len(Ingredients))}
ingrProteinP = {Ingredients[i]: ingrProtein[Ingredients[i]]*4*(1/ingrCals[Ingredients[i]]) for i in range(len(Ingredients))}

print("Ingredient carbs percent: \n", ingrCarbsP)
print("Ingredient fat percent: \n", ingrFatP)
print("Ingredient protein percent: \n", ingrProteinP)

#origPercent = dict.fromkeys(Ingredients,[origWeight[i] / sum(origWeight.values()) for i in Ingredients])
#ingrCarbsP = dict.fromkeys(Ingredients,[ingrCarbs[i]*4*(1/100) for i in Ingredients])
#ingrFatP = dict.fromkeys(Ingredients,[ingrFat[i]*9*(1/100) for i in Ingredients])
#ingrProteinP = dict.fromkeys(Ingredients,[ingrProtein[i]*4*(1/100) for i in Ingredients])

#exit()
# constants
cals_gram = np.array([1/4, 1/9, 1/4]) # the grams of carbs, fat, protein

# calculated parameters
#M = u*O*cals_gram   # the macros in grams (carbs, fat, protein)
carbsGrams = userCals*macrosPercent[0]*(1/4)
fatGrams = userCals*macrosPercent[1]*(1/9)
proteinGrams = userCals*macrosPercent[2]*(1/4)
uCarbsP = macrosPercent[0]
uFatP = macrosPercent[1]
uProteinP = macrosPercent[2]

# used this link: https://towardsdatascience.com/linear-programming-and-discrete-optimization-with-python-using-pulp-449f3c5f6e99
# problem
prob = LpProblem("The Reisso Problem", LpMinimize)

# A dictionary called 'ingredient_vars' is created to contain the referenced Variables
#optWeights = LpVariable.dicts("Ingr",Ingredients,0)
optPercent = LpVariable.dicts("Ingr",Ingredients,0)
print(optPercent)
calsDif = LpVariable("Calorie Difference",0)
carbsDif = LpVariable("Carbs Difference", 0)
fatDif = LpVariable("Fat Difference", 0)
proteinDif = LpVariable("Protein Difference", 0)
#percentDif = LpVariable.dicts("Percent Ingrediant Difference",Ingredients,0)

reicipeDif = LpVariable.dicts("Reicipe Diff",Ingredients,0)

#print("Original Percent Type:",type(origPercent["Steak"]),". Value: ", origPercent["Steak"])
#print("Reicipe Diff Type:",type(reicipeDif["Steak"]), ". Value: ", reicipeDif["Steak"])
#print("Opt Percent Type:",type(optPercent["Steak"]),". Value: ", optPercent["Steak"])
#exit()
# objective function
#prob += lpSum(calsDif + lpSum(percentDif[i] for i in Ingredients) + carbsDif + fatDif + proteinDif)
#prob += lpSum(0.1*lpSum(reicipeDif for i in Ingredients) + carbsDif + fatDif + proteinDif)
prob += lpSum(carbsDif + fatDif + proteinDif)

prob += lpSum([optPercent[i] for i in Ingredients]) == 1, "PercentagesSum"
for i in Ingredients:
    prob += reicipeDif[i] >= origPercent[i] - optPercent[i]
    prob += reicipeDif[i] >= optPercent[i] - origPercent[i]

# constraints
#prob += calsDif >= lpSum([optWeights[i]*ingrCals[i]*(1/100) for i in Ingredients]) - userCals
#prob += calsDif >= -lpSum([optWeights[i]*ingrCals[i]*(1/100) for i in Ingredients]) + userCals
prob += carbsDif + uCarbsP >= lpSum([optPercent[i]*ingrCarbsP[i] for i in Ingredients])
prob += carbsDif - uCarbsP >= -lpSum([optPercent[i]*ingrCarbsP[i] for i in Ingredients])
prob += fatDif + uFatP >= -lpSum([optPercent[i]*ingrFatP[i] for i in Ingredients])
prob += fatDif - uFatP >= lpSum([optPercent[i]*ingrFatP[i] for i in Ingredients])
prob += proteinDif + uProteinP >= -lpSum([optPercent[i]*ingrProteinP[i] for i in Ingredients])
prob += proteinDif - uProteinP >= lpSum([optPercent[i]*ingrProteinP[i] for i in Ingredients])
#for i in Ingredients:
    #prob += percentDif[i] >= origWeight[i]*(1/lpSum([origWeight[j] for j in Ingredients])) - optWeights[i]*1/lpSum([optWeights[j] for j in Ingredients])
    #prob += percentDif[i] >= -origWeight[i]*(1/lpSum([origWeight[j] for j in Ingredients])) + optWeights[i]*1/lpSum([optWeights[j] for j in Ingredients])
#    prob += percentDif[i]*sum(origWeight.values())*lpSum([optWeights[j] for j in Ingredients]) >= origWeight[i]*lpSum([optWeights[j] for j in Ingredients]) - optWeights[i]*sum(origWeight.values())
#    prob += percentDif[i]*sum(origWeigt.values())*lpSum([optWeights[j] for j in Ingredients]) >= -origWeight[i]*lpSum([optWeights[j] for j in Ingredients]) + optWeights[i]*sum(origWeight.values())
# The problem data is written to an .lp file
prob.writeLP("ReissoModel.lp")

# The problem is solved using PuLP's choice of Solver
prob.solve()

# The status of the solution is printed to the screen
print("Status:", LpStatus[prob.status])

# Each of the variables is printed with it's resolved optimum value
for v in prob.variables():
    print(v.name, "=", v.varValue)


print("The original Percentages: \n", origPercent)
print("Optimum Percentages: \n", optPercent)